const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors'); 
require('dotenv').config();

const users = require('./routes/users');
const products = require('./routes/products');
const orders = require('./routes/orders');


const app = express();
const port = process.env.PORT;


app.use(express.json());
app.use(express.urlencoded({extended:true}));


mongoose.connect(process.env.DB_MONGODB_ATLAS , {useNewUrlParser:true, useUnifiedTopology:true});

let db = mongoose.connection;


app.use('/api', users);
app.use('/api', products);
app.use('/api', orders);


db.on('error', console.error.bind(console, "Connection error!"));
db.on('open', () => console.log("We are connected to the database!"));


app.get('/', (req, res) => {
	res.send("Landing Page");
});


app.listen(port, () => console.log(`Your server is listening to port ${port}`));
