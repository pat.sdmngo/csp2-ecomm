const mongoose = require('mongoose');
const Order = require('./orderModel')

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required']
	},
	email: {
		type: String,
		required: [true, 'Email is required']
	},
	password: {
		type: String,
		required: [true, 'Password is required']
	},
	mobileNo: {
		type: String,
		required: [true, 'Contact number is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [{
		type: Array
	}]
});

module.exports = mongoose.model('User', userSchema);
