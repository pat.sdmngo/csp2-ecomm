const Order = require('../models/orderModel');
const User = require('../models/userModel');
const Product = require('../models/productModel');
const Cart = require('../models/cartModel');


/* ----- CUSTOMER ----- */

// function for adding an order to the cart
module.exports.addToCart = (req, res) => {
	let userId = req.user.id;
	let product = req.body.product;

	Product.find({_id: product})
		.then(() => {
			let newCart = new Cart({
				user: userId,
				products: product
			})
			newCart.save().then(newCart => {
				Cart.find({_id: newCart._id})
					.populate({path: 'products', populate: { path: 'products' }})
					.then(yourCart => {res.send({
						message: 'Item succesfully added to your cart!', yourCart
					})
				})
			});

		}).catch(err => res.send(err))
};


// function to retrieve a cart
module.exports.findCart = (req, res) => {
	let userId = req.user.id;

	Cart.find({user: userId})
	.populate({path: 'products', populate: { path: 'products' }})
	.then(result => {
		// const products = result.reduce((initial, current) => {
		// 	return [...initial, ...current.products];
		// }, []);

		res.send({
			yourCart: result
		}); 
	}).catch(err => res.send(err))
};


// function to proceed to checkout
module.exports.checkOutOrder = (req, res) => {
	let userId = req.user.id;
	let confirm = req.body.confirmation;
	let shippingAddress = req.body.shippingAddress;

	Cart.find({user: userId})
	.populate({path: 'products', populate: { path: 'products' }})
	.then(foundCart => {
		let filteredCart = foundCart.filter(item => {return item.status == 'Pending'});

		const products = filteredCart.reduce((initial, current) => {
			return [...initial, ...current.products];
		}, []);

		let calculate = products.map(product => {
			return product.description.quantity * product.price
		})
		
		let totalAmount = calculate.reduce((initial, current) => {
			return initial + current
		}, 0)

		if (!confirm) {
			res.send('You will be redirected to back to your cart.')
		} else {
			if (shippingAddress == null || shippingAddress == undefined || shippingAddress.length == 0) {
				res.send('Please fill up your shipping address.')
			} else {
				User.find({_id: userId})
				.populate({path: 'orders', populate: { path: 'orders' }})
				.then(() => {
					let newOrder = new Order({
						orderedBy: userId,
						shippingAddress: shippingAddress,
						cart: products,
						totalAmount: totalAmount
					})
					newOrder.save().then(newOrder => {
						let id = newOrder.orderedBy;
						let update = {$set: {orders: newOrder}};
						let options = {new: true}
						User.findByIdAndUpdate(id, update, options)
						.then(user => {
							Cart.updateMany({user: userId}, {status: "Purchased"})
							.then(result => {
								let today = new Date();
								let nextWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7);
								if (newOrder.cart.length === 0) {
									res.send('Your cart is empty!')
								} else {
									res.send({
										message: "Order successful!",
										shippingFee: totalAmount * 0.10,
										deliveryDate: nextWeek,
										orderDetails: newOrder
									})	
								}
							})
							.catch(err => res.send(err))
						}).catch(err => res.send(err))
					})
				}).catch(err => res.send(err))
			}
		}

	}).catch(err => res.send(err));
};


// function to view orders as a customer
module.exports.showOrders = (req, res) => {
	let userId = req.user.id;
	Cart.find({user: userId})
	.populate({path: 'products', populate: { path: 'products' }})
	.then(foundCart => {
		Order.find({orderedBy: userId})
		.populate({path: 'orderedBy', populate: { path: 'orderedBy' }})
		.then(orders => {
			let viewOrder = orders.map(order => {
				return {
					purchasedOn: order.purchasedOn,
					cart: foundCart,
					_id: order._id,
					shippingAddress: order.shippingAddress,
					totalAmount: order.totalAmount
				}
			})
			res.send({orders: viewOrder})
		})
	}).catch(err => res.send(err))
};


// function to delete an item from the cart
module.exports.removeItem = (req, res) => {
	let userId = req.user.id;
	Cart.find({user: userId})
	.populate({path: 'products', populate: { path: 'products' }})
	.then(foundCart => {
		let cartId = req.body.cartId;
		Cart.findByIdAndDelete(cartId)
		.then(() => {
			Cart.find({user: userId})
			.populate({path: 'products', populate: { path: 'products' }})
			.then(result => res.send({message: 'Item successfully removed from your cart', yourCart: result}))			
		}).catch(err => res.send(err))
	}).catch(err => res.send(err))
}


/* ----- ADMIN ----- */

// function to retrieve all orders
module.exports.allOrders = (req, res) => {
	let userId = req.user.id;
	Cart.find({})
	.populate({path: 'products', populate: { path: 'products' }})
	.then(foundCart => {
		Order.find({})
		.populate({path: 'orderedBy', populate: { path: 'orderedBy' }})
		.then(orders => {
			const result = foundCart.reduce((initial, current) => {
				return [...initial, ...current.products];
			}, []);
			let viewOrder = orders.map(order => {
				return {
				purchasedOn: order.purchasedOn,
				cart: result,
				_id: order._id,
				orderedBy: {
					firstName: order.orderedBy.firstName,
					lastName: order.orderedBy.lastName,
					email: order.orderedBy.email,
					mobileNo: order.orderedBy.mobileNo,
				},
				shippingAddress: order.shippingAddress,
				totalAmount: order.totalAmount
			}
			})
			res.send({orders: viewOrder})
		})
	}).catch(err => res.send(err))
};



