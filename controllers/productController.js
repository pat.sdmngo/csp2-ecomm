const Product = require('../models/productModel');

/* ----- GENERAL ----- */

// function for retrieving all active products
module.exports.showAllProducts = (req, res) => {
	Product.find({isActive: true})
	.then(products => res.send({searchResults: products}))
	.catch(err => res.send({message: 'An error occurred.', details: err}));
}

// function for retrieving products by collection
module.exports.showByCollection = (req, res) => {
	Product.find({collectionName: req.body.collection})
	.then(results => {
		if (results.length === 0) {
			res.send('No results found!')
		} else {
			res.send({searchResults: results})
		}
	})
	.catch(err => res.send({message: 'An error occurred.', details: err}));
}

// function for retrieving a specific product
module.exports.showProductDetails = (req, res) => {
	let productId = req.params.productId;
	Product.findById(productId)
	.then(showProduct => res.send(showProduct))
	.catch(err => res.send({message: 'An error occurred.', details: err}));
}


/* ----- ADMIN ----- */

// function for adding a product
module.exports.addProduct = (req, res) => {
	let name = req.body.name;
	let description = req.body.description;
	let collectionName = req.body.collectionName;
	let price = req.body.price;
	let isActive = req.body.isActive;
	let createdOn = req.body.createdOn;

	let newProduct = new Product ({
		name: name,
		description: description,
		collectionName: collectionName,                                                                                                                                                                                                                                                                   
		price: price,
		isActive: isActive,
		createdOn: Date.now()
	});

	newProduct.save()
	.then(newProduct => res.send({message: 'New product has been added!', product: newProduct}))
	.catch(err => res.send({message: 'An error occurred.', details: err}));
}

// function for updating a product
module.exports.updateProduct = (req, res) => {
	let productId = req.params.productId;
	let body = req.body;

	// let updates = {$set: {
	// 	name: body.name,
	// 	description: body.description,
	// 	collectionName: body.collectionName,
	// 	price: body.price
	// }};
	
	let updates = {$set: req.body};
	let options = { new: true};

	Product.findByIdAndUpdate(productId, updates, options)
	.then(update => res.send(update))
	.catch(err => res.send({message: 'An error occurred.', details: err}));
}

// function for archiving a product
module.exports.archiveProduct = (req, res) => {
	let productId = req.params.productId;

	Product.findOne({_id: productId})
	.then(product => {
		if (product.isActive === true) {
			product.isActive = false;
			product.save()
			.then(archived => {res.send({message: 'Product archived successfully.', archived})})
			.catch(err => res.send(err))
		} else {
			res.send({message: 'Product is already archived.'})
		}
	}).catch(err => res.send({message: 'An error occurred.', details: err}));
};

// function to retrieve all archived products
module.exports.getArchivedProducts = (req, res) => {
	Product.find({isActive: false})
	.then(products => res.send(products))
	.catch(err => res.send(err))
}

// function to retrieve all products
module.exports.getAllProducts = (req, res) => {
	Product.find({})
	.then(products => res.send(products))
	.catch(err => res.send({message: 'An error occurred.', details: err}));
}





