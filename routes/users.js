const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');
const auth = require('../auth');


/* ----- GENERAL ----- */

// user registration
router.post('/register', userController.userRegistration);

// user login
router.post('/login', userController.userLogin);

// user profile
router.get('/profile', auth.verify, userController.getUserProfile);

// update user profile
router.put('/profile/update', auth.verify, userController.updateUserProfile);


/* ----- ADMIN ----- */

// get all users
router.get('/users', auth.verify, auth.adminVerification, userController.getAllUsers);

// set user as admin
router.put('/profile/admin', auth.verify, auth.adminVerification, userController.makeAdmin);



/* ============================================================== */

/* ----- ADDITIONAL FEATURES ----- */


// user account deletion
router.delete('/profile/delete', auth.verify, userController.deleteAccount)


module.exports = router;