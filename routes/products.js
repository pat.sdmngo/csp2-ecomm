const express = require('express');
const router = express.Router();

const productController = require('../controllers/productController')
const auth = require('../auth');


/* ----- GENERAL ----- */

// get all active products
router.get('/products/active', auth.verify, productController.showAllProducts);

// get all active products
router.get('/products/active/collections', auth.verify, productController.showByCollection);

// show product details
router.get('/products/:productId', auth.verify, productController.showProductDetails);


/* ----- ADMIN ----- */

// add a product
router.post('/products', auth.verify, auth.adminVerification, productController.addProduct); // marked: need to add authentication

// update a product
router.put('/products/update/:productId', auth.verify, auth.adminVerification, productController.updateProduct);

// archive a product
router.put('/products/archive/:productId', auth.verify, auth.adminVerification, productController.archiveProduct);

// get all archived products
router.get('/archive', auth.verify, auth.adminVerification, productController.getArchivedProducts);

// get all products
router.get('/products', auth.verify, auth.adminVerification, productController.getAllProducts);



module.exports = router;